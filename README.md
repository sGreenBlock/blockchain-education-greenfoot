# Simple Blockchain for educational purpose

*Greenfoot* implementation of a simple Blockchain algorithm, loosely oriented on the Bitcoin algorithm. Developed at the *HAK für Wirtschaftsinformatik - digBiz HAK Imst*.

Mining is based on Hashcode guessing. Difficulty is based on predefined part of Hashcode and can be adjusted.

Educational purpose was kept in mind to teach the important basics of a
blockchain without getting to complicated.

## Concepts

* Create a list of Objects holding data 
* Create the blockchain interconnecting this Objects by hashcodes
* Implement a simple version of a proof of work algorithm

![Demo of blockchain for education simple](images/blockchain-education-greenfoot.gif)

## Educational Tipps

* If you want to implement the code with students think of removing everything except the methods `Block.calculateHash()` and `Block.drawBlockImage()`. These methods are overhead which is not needed for Blockchain understanding.
* If you need a good explanation beyond the comments, refer to section *References*.
* Let advanced students add concurrency to the mining part.

## Installation and usage

All attendees have to

* install [Greenfoot](https://www.greenfoot.org/download)

Offer the students as much code as you want. If you want to test this example, you have to

* Download the code and open it in Greenfoot
* Create a new BlockChain Object in Greenfoot
* Blocks are created by Left clicking the BlockChain Object and selecting `addBlock(String data)`. The data String has no function. You can add data of your choice.
* Enjoy watching the hash codes and inspecting the code

## Coke and Pizza

As soon as you are ready with education, put the coffee to your side, take some coke and pizza and go on with [sGreenBlock Light](https://gitlab.com/sGreenBlock/sgreenblock-light). The game is ready and pending for public availabilty. Some last work to publish it has to be done. Stay tuned and ask for it, if you can not wait.

The game sGreenBlockLight expands the code to a cool game. You can not only mine new blocks and get coins for successful mining (be aware that someone can be faster!). You are also able to make smart contracts about Blockchain size in a given time. If you win a contract, you will earn coins! But a contract is only active, if it is listed in a mined block. The Blockchain is synced among all attendees. Do not hold back to exchange the coins for something in the end.

Don't burn your CPUs!

## References

Losoely based on part 1 of the brilliant Java tutorial [Creating Your First Blockchain with Java](https://medium.com/programmers-blockchain/create-simple-blockchain-java-tutorial-from-scratch-6eeed3cb03fa).

## License

    Blockchain Education Greenfoot
    Copyright (C) 2018  sGreenBlock

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.