/*
    Blockchain Education Greenfoot
    Copyright (C) 2018  sGreenBlock

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import greenfoot.*; 
import java.util.Date;
import java.security.*;

/**
 * The Block class is a simplification of real BlockChain Blocks
 * used in Bitcoin algorithm. 
 * 
 * @author st.stolz
 */
public class Block extends Actor
{
    /**
     * The hash of this block also integrating previousHash (Merkle Tree)
     */
    public String hash;
    /**
     * The Hash of previous block
     */
    public String previousHash;
    /**
     * Simple string which fakes data stored with block (for example
     * smart contracts)
     */
    private String transactionData; 
    /**
     * When was block created?
     */
    private long timeStamp; 
    /**
     * The nonce is needed for mining
     */
    private int nonce=0;

    /**
     * Creates a Block object. But keep in mind, that it has to be mined to 
     * be ready. 
     * 
     * @param transactionData simple String of choice to fake Block data
     * @param previousHash the hash of previous block
     */
    public Block(String transactionData, String previousHash) {
        this.transactionData = transactionData;
        this.previousHash = previousHash;
        this.timeStamp = new Date().getTime();
        this.hash = calculateHash();
        drawBlockImage();
    }
    
    /**
     * Draws the image for Greenfoot with the hash of this
     * block on it.
     */
    public void drawBlockImage(){
        GreenfootImage thisImage = getImage();
        thisImage.drawString(this.hash,2,thisImage.getHeight()/2);
        this.setImage(thisImage);
       }
    
    /**
     * Example how a hash could be generated without using a 
     * hashing algorithm from lib. Yout can try it, but
     * keep in mind that mining could get difficult, because this
     * algorithm is not that random as Algorithms like SHA.
     * 
     * @return the resulting hash.
     */
    public String myCalculateHash(){
        int result = 17;
        result = 31 * result + previousHash.hashCode();
        result = 31 * result + (int) timeStamp;
        result = 31 * result + (int) transactionData.hashCode();
        result = 31 * result + nonce;
        result = 31 * result;
        return Integer.toString(result);
    }

    /**
     * Packages from java.security used to generate a SHA-256 hash
     */
    public String calculateHash(){
        String strToHash = previousHash + timeStamp + transactionData + nonce;
        try{
            byte[] bytesOfMessage = strToHash.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < thedigest.length; i++) {
                String hex = Integer.toHexString(0xff & thedigest[i]);
    			if(hex.length() == 1) 
    			     hexString.append('0');
    			hexString.append(hex);
            }
            return hexString.toString();
        }
        catch(Exception e) {
			throw new RuntimeException(e);
		}
        
    }
    /**
     * 
     * @return interesting data about this block
     */
    public String toString(){
        String returnStr = "";
        returnStr += String.format("\n %15s: %s","hash",hash);
        returnStr += String.format("\n %15s: %s","previousHash",previousHash);
        returnStr += String.format("\n %15s: %s","transactionData",transactionData);
        returnStr += String.format("\n %15s: %s","timeStamp",timeStamp);
        return returnStr;
    }
    
    /**
     * Setter for transactionData
     * @param transactionData
     */
    public void setTransactionData(String transactionData){
        this.transactionData = transactionData;
    }
    
    /**
     * Start mining of this block. Difficulty corresponds to the number 
     * of trailing zeros which the generated hash must have. Difficulties
     * >3 are getting hard to generate. The real Bitcoin algorithm will
     * increase zeros bitwise to get a slower gain of difficulty. 
     * @param difficulty higher value higher difficulty
     */
    public void mineBlock(int difficulty) {
        String target = new String(new char[difficulty]).replace('\0', '0'); //Create a string with difficulty * "0" 
        long counter = 0;
        while(counter < 1147483647) {
            nonce ++;
            hash = calculateHash();
            counter++;          
            if(hash.substring( 0, difficulty).equals(target)){
                System.out.println("Block Mined with : " + counter + " tries");
                System.out.println("Hash: " + hash);
                System.out.println("nonce: " + nonce);
                break;
            }
            else{
                System.out.println("target : " + target + ", hash: " + hash);
            }
        }
        System.out.println("Mining over!");
        
    }
}
