/*
    Blockchain Education Greenfoot
    Copyright (C) 2018  sGreenBlock

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import greenfoot.*; 
import java.util.ArrayList;

/**
 * The BlockChain class is used to store the Blockchain of the game.
 * The application is used with the methods of this class. 
 * 
 * @author (st.stolz) 
 */
public class BlockChain extends Actor
{
    /**
     * The blockchain list holds all blocks which were generated.
     */
    public static ArrayList<Block> blockchain; 
    
    public BlockChain(){
        blockchain = new ArrayList<Block>(); 
    }
    
    /**
     * Create new block, mine it and add to list. 
     */
    public void addBlock(String data){
        String perviousHash = "0";
        int perviousHeight = 0;
        if(blockchain.size() > 0){
            Block previousBlock = blockchain.get(blockchain.size()-1);
            perviousHeight = previousBlock.getImage().getHeight();
            perviousHash = previousBlock.hash;
        }     
        Block newBlock = new Block(data, perviousHash);
        World world = getWorld();
        
        world.addObject(newBlock,this.getX()+50,this.getY()+((perviousHeight/2)*blockchain.size()));
        
        newBlock.mineBlock(4);
        
        blockchain.add(newBlock);
    }
    
    /**
     * Print all blocks and if chain is valid
     */
    public void printChain(){
        String outputStr = "";
        for(Block block : blockchain){
            System.out.println(block.toString());
        }
        if(isChainValid()){
            System.out.println("The Chain is valid!");
        }
        else{
            System.out.println("CHAIN CORRUPTED!");
        }
    }
    
    /**
     * Check if chain is valid. Walk through all blocks and
     * check if this and previous hash stored in block corresponds
     * to calculated value. 
     */
    public boolean isChainValid(){
        boolean returnCheck = true;
        
        Block currentBlock;
        Block previousBlock;
        
        for (int i = 1; i < blockchain.size();i++){
            currentBlock = blockchain.get(i);
            previousBlock = blockchain.get(i-1);
            
            if(!currentBlock.hash.equals(currentBlock.calculateHash()) ){
              returnCheck = false;
            }
            
           if(!previousBlock.hash.equals(currentBlock.previousHash) ) {
              returnCheck = false; 
           }
        }
        return returnCheck;
    }
    
}
