import greenfoot.*; 

/**
 * MyWorld class as installed by Greenfoot. No
 * change needed here.
 */
public class MyWorld extends World
{

    public MyWorld()
    {    
        super(600, 400, 1); 
    }

    private void prepare()
    {
        BlockChain blockchain = new BlockChain();
        addObject(blockchain,112,172);
        blockchain.addBlock("2");
    }
}
